#ifndef STOPPER_HPP
#define STOPPER_HPP

#include <atomic> // std::atomic_bool
#include <memory> // std::shared_ptr

class Stopper {
public:
    Stopper();
    Stopper(const Stopper&) = default;

    operator bool() const;
    void stop();

private:
    std::shared_ptr<std::atomic_bool> flag;
};

#endif // STOPPER_HPP
