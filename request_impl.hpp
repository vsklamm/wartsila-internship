#ifndef REQUEST_IMPL_HPP
#define REQUEST_IMPL_HPP

#include <string>

struct Request {
    Request(const std::string& msg);

    std::string message;
};

#endif // REQUEST_IMPL_HPP
