# Wärtsilä Internship

## Producer-consumer problem written in C++ (C++14)

### Requirements
`cmake, make, gcc (with C++14 support)`

### Run
```bash
mkdir build-release && cd build-release && cmake .. -DCMAKE_BUILD_TYPE=Release && make
./wartsila
```
