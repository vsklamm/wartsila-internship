#ifndef PARALLEL_MAPPER_HPP
#define PARALLEL_MAPPER_HPP

#include <boost/lockfree/queue.hpp> // boost::lockfree::queue
#include <boost/thread/thread.hpp> // boost::thread_group

#include <cstddef> // size_t

#include "api.hpp"

class parallel_mapper {
public:
    parallel_mapper(size_t consumer_thread_count);
    parallel_mapper(parallel_mapper const&) = delete;
    ~parallel_mapper();

    void run_producer();
    void stop_all();

private:
    void poll_tasks();

    boost::thread_group producer_threads;
    boost::thread_group consumer_threads;
    boost::lockfree::queue<Request*> requests;
    Stopper stopper;
};

#endif // PARALLEL_MAPPER_HPP
