#include <atomic> // std::atomic_int
#include <iostream> // std::cout, std::endl
#include <mutex> // std::mutex
#include <string> // std::to_string
#include <thread> // std::this_thread

#include "api.hpp"

std::mutex mutex;
std::atomic_int counter { 0 };

void ProcessRequest(Request* request, Stopper stopSignal)
{
    if (stopSignal) {
        return;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(800)); // or any work with check for stopSignal
    if (stopSignal) {
        return;
    }
    std::lock_guard<std::mutex> lock(mutex);
    std::cout
        << "[" << std::this_thread::get_id()
        << "] processed: "
        << request->message << std::endl;
}

Request* GetRequest(Stopper stopSignal)
{
    if (stopSignal) {
        return nullptr;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(400));
    if (stopSignal) {
        return nullptr;
    }
    std::lock_guard<std::mutex> lock(mutex);
    std::cout
        << "[" << std::this_thread::get_id()
        << "] get: "
        << counter << std::endl;
    return (stopSignal) ? nullptr : new Request { std::to_string(counter++) };
}

void delete_request(Request* request)
{
    const auto msg = request->message;
    delete request;
    std::lock_guard<std::mutex> lock(mutex);
    std::cout
        << "[" << std::this_thread::get_id()
        << "] deleted: "
        << msg << std::endl;
}
