#ifndef API_HPP
#define API_HPP

#include "request_impl.hpp"
#include "stopper.hpp"

Request* GetRequest(Stopper stopSignal);

void ProcessRequest(Request* request, Stopper stopSignal);

void delete_request(Request* request);

#endif // API_HPP
