#include "parallel_mapper.hpp"

#include <exception> // std::runtime_error
#include <iostream> // std::cout, std::endl
#include <memory> // std::shared_ptr

parallel_mapper::parallel_mapper(size_t consumer_thread_count)
    : requests { 128 }
{
    if (consumer_thread_count < 2) {
        throw std::runtime_error("Thread pool should contain at least two threads");
    }
    try {
        for (size_t i = 0; i != consumer_thread_count; ++i) {
            consumer_threads.create_thread([this]() {
                Request* task;
                while (!stopper) {
                    while (requests.pop(task)) {
                        ProcessRequest(task, stopper);
                        delete_request(task);
                    }
                }
                while (requests.pop(task)) {
                    delete_request(task);
                }
            });
        }
    } catch (...) {
        stop_all();
        throw std::runtime_error("Error while creating thread occurred");
    }
}

parallel_mapper::~parallel_mapper()
{
    stop_all();
    producer_threads.join_all();
    consumer_threads.join_all();
}

void parallel_mapper::run_producer()
{
    producer_threads.create_thread([this]() {
        while (!stopper) {
            auto request = GetRequest(stopper);
            if (request != nullptr) {
                while (!requests.push(request))
                    ;
            }
        }
    });
}

void parallel_mapper::stop_all()
{
    stopper.stop();
    std::cout << "stopped" << std::endl;
}
