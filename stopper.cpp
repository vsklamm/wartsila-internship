#include "stopper.hpp"

Stopper::Stopper()
    : flag { new std::atomic_bool { false } }
{
}

void Stopper::stop()
{
    *flag = true;
}

Stopper::operator bool() const
{
    return *flag;
}
