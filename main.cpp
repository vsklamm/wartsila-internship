#include <chrono> // std::chrono::seconds
#include <thread> // std::this_thread::sleep_for

#include "parallel_mapper.hpp"

int main()
{
    const auto concurrent_threads = std::thread::hardware_concurrency();
    parallel_mapper mapper(concurrent_threads);
    mapper.run_producer();
    // mapper.run_producer(); // many producers possible

    std::this_thread::sleep_for(std::chrono::seconds(30));
    mapper.stop_all(); // or without it
}
